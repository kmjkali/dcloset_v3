package com.example.DCloset.entity;

import com.example.DCloset.enums.DeliveryType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.time.LocalDate;

@Getter
@Setter
@Entity

public class Delivery {

    //order 에연결


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ordersId")
    private Orders orders;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private DeliveryType deliveryType;

    @Column(nullable = false)
    private LocalDate deliveryDate;

    @Column(nullable = false,length = 30)
    private String deliveryNumber;


}
