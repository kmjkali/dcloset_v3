package com.example.DCloset.entity;

import com.example.DCloset.enums.PayStatus;
import com.example.DCloset.enums.PayWay;
import com.example.DCloset.enums.PaymentAmount;
import com.example.DCloset.enums.RefundAmount;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity

public class Charge {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDateTime actualPayDay;

    @Column(nullable = true)
    private LocalDateTime refundDay;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false,length = 20)
    private PayStatus payStatus;

    @Column(nullable = false)
    private String rentalType;

    @Column(nullable = false)
    private Double paymentAmount;

    @Column(nullable = false)
    private Double refundAmount;

    @Column(nullable = false)
    private PayWay payWay;

    @Column(nullable = false, length = 40)
    private String payInfo;

}
