package com.example.DCloset.entity;

import com.example.DCloset.enums.QuestionStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class QuestionBulletin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER )
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate questionCreateDate;

    @Column(nullable = false)
    private String questionTitle;

    @Column(nullable = false)
    private Integer questionPassword;

    @Column(columnDefinition = "Text",nullable = false)
    private String questionContent;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private QuestionStatus questionStatus;






}
