package com.example.DCloset.service;

import com.example.DCloset.enums.ResultCode;
import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        result.setCode(ResultCode.SUCCES.getCode());
        result.setMsg(ResultCode.SUCCES.getMsg());

        return result;
    }

    public static <T> SingResult<T> getSingResult(T data) {
        SingResult<T> result = new SingResult();
        result.setCode(ResultCode.SUCCES.getCode());
        result.setMsg(ResultCode.SUCCES.getMsg());
        result.setData(data);

        return result;
    }

    public static <T>ListResult<T> getListResult(List<T> list) {
        ListResult<T> result = new ListResult<>();
        result.setCode(ResultCode.SUCCES.getCode());
        result.setMsg(ResultCode.SUCCES.getMsg());
        result.setList(list);
        result.setTotalCount(list.size());

        return result;
    }
}
