package com.example.DCloset.service;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.RentalType;
import com.example.DCloset.model.order.*;
import com.example.DCloset.repository.MemberRepository;
import com.example.DCloset.repository.OrderRepository;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.enums.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final MemberRepository memberRepository;

    public Orders getOrders(long id) {return orderRepository.findById(id).orElseThrow();}

    //주문 정보 등록

    public void setOrder(Member member, Goods goods, OrderRequest orderRequest) throws Exception{

        Orders addData = new Orders();
        Member minusRemain = memberRepository.findById(member.getId()).orElseThrow();
        LocalDate deadLineDate = orderRequest.getDesiredDate().plusDays(10);
        addData.setMember(member);
        addData.setGoods(goods);
        addData.setOrderDate(LocalDate.now());
        addData.setRentalType(orderRequest.getRentalType());

        // 멤버십 주문 이나 무료 대여 시 횟수 차감 하는 기능.
        // 주문할 경우 횟수 차감된다는 안내 문구 처리하기.
        // 만약 잔여 횟수가 0회 이하라면 예외처리. 렌탈 가능 횟수가 없습니다.
        if (orderRequest.getRentalType().equals(RentalType.MEMBERSHIP) || orderRequest.getRentalType().equals(RentalType.EXPERIENCE)) {
            minusRemain.setRemainTime(minusRemain.getRemainTime() - 1);
            if (minusRemain.getRemainTime() <= -1) throw new Exception();
        }

        switch (orderRequest.getRentalType()) {
            case MEMBERSHIP:
                if (goods.getYnMembership().equals(false)) throw new Exception();
                break;
            case OFFLINE:
                if (goods.getYnOffline().equals(false)) throw new Exception();
                break;
            case EXPERIENCE:
                if (goods.getYnFree().equals(false)) throw new Exception();
                break;
            case DAILY:
                if (goods.getYnOneDay().equals(false)) throw new Exception();
                break;
        }

        // 만약 대여 불가능한 렌탈 타입을 주문했을 경우 예외 처리.

        addData.setGoodsSize(orderRequest.getGoodsSize());
        addData.setGoodsColor(orderRequest.getGoodsColor());
        addData.setDesiredDate(orderRequest.getDesiredDate());

        // 만약 희망 수령 일자가 오늘 날짜 보다 이전일 경우 예외 처리.
        if (orderRequest.getDesiredDate().isBefore(LocalDate.now())) throw new Exception();
        addData.setDeadlineDate(deadLineDate);

        // 기본값 주문 접수로 고정
        addData.setOrderStatus(OrderStatus.RECEIVE);

        orderRepository.save(addData);
        memberRepository.save(minusRemain);
    }


    //주문 정보 전체 조회 복수 R

    public List<OrderItem> getOrders() {
        List<Orders> originList = orderRepository.findAll();
        List<OrderItem> result = new LinkedList<>();

        for(Orders orders : originList) {
            OrderItem addItem = new OrderItem();

            addItem.setId(orders.getId());
            addItem.setMember(orders.getMember());
            addItem.setGoods(orders.getGoods());
            addItem.setGoodsSize(orders.getGoodsSize());
            addItem.setRentalType(orders.getRentalType().getRentalType());
            addItem.setGoodsColor(orders.getGoodsColor());
            addItem.setOrderDate(orders.getOrderDate());
            addItem.setDesiredDate(orders.getDesiredDate());
            addItem.setDeadlineDate(orders.getDeadlineDate());
            addItem.setOrderStatus(orders.getOrderStatus());

            result.add(addItem);
        }

        return result;
    }

    //주문 정보 주문 ( id 로 조회)

    public OrderResponse getOrder(long id) {
        Orders originData = orderRepository.findById(id).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setId(originData.getId());
        response.setGoods(originData.getGoods());
        response.setMember(originData.getMember());
        response.setRentalType(originData.getRentalType().getRentalType());
        response.setOrderDate(originData.getOrderDate());
        response.setGoodsColor(originData.getGoodsColor());
        response.setGoodsSize(originData.getGoodsSize());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }


    //주문 정보 회원 아이디별 ( member Id 로  조회 ) , 단수 R
    //단수R이 아니라 리스트로 구현해보자.

    public OrderResponse getMemberOrder(Member member){

        Orders originData = orderRepository.findById(member.getId()).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setMember(originData.getMember());
        response.setGoods(originData.getGoods());
        response.setId(originData.getId());
        response.setRentalType(originData.getRentalType().getRentalType());
        response.setGoodsSize(originData.getGoodsSize());
        response.setGoodsColor(originData.getGoodsColor());
        response.setOrderDate(originData.getOrderDate());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }

    // member id로 주문 수정하기
    public void putOrderChangeMemberRequest(Member member , OrderChangeMemberRequest request){
        Orders originData = orderRepository.findById(member.getId()).orElseThrow();

        originData.setGoodsSize(request.getGoodsSize());
        originData.setGoodsColor(request.getGoodsColor());

        orderRepository.save(originData);

    }
    // id로 주문 상태 수정하기
    public void putOrderChangeRequest(long id, OrderChangeStatusRequest request){
        Orders originData = orderRepository.findById(id).orElseThrow();

        originData.setOrderStatus(request.getOrderStatus());
    }

    public long getNewOrderList() {
        long countNewOrders = orderRepository.countByOrderStatus(OrderStatus.RECEIVE);
        return countNewOrders;
    }



    //member id로 주문 상태를 변경 할 수 있는 기능

    public void putOrderChangeStatusRequest(Member member, OrderChangeStatusRequest request){
        Orders originData = orderRepository.findById(member.getId()).orElseThrow();

        originData.setOrderStatus(request.getOrderStatus());

        orderRepository.save(originData);
    }





    //주문 취소 기능 (삭제  = 주문을 삭제하고 ,주문할 수 없는 상품입니다. 라고 보여준다)
    // 멤버십 주문이나 무료대여의 경우 주문할 때 차감됐던 렌탈 횟수를 돌려준다.

    public void delOrder(long id){
        Orders originData = orderRepository.findById(id).orElseThrow();
        if (originData.getRentalType().equals(RentalType.MEMBERSHIP) || originData.getRentalType().equals(RentalType.EXPERIENCE)) {
            originData.getMember().setRemainTime(originData.getMember().getRemainTime() + 1);
        }
        orderRepository.deleteById(id);
        memberRepository.save(originData.getMember());
    }


}
