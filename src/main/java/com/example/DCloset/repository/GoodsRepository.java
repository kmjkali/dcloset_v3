package com.example.DCloset.repository;
import com.example.DCloset.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsRepository extends JpaRepository<Goods,Long> {
}
