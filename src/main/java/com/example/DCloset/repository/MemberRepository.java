package com.example.DCloset.repository;

import com.example.DCloset.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.time.Month;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);
    long countMemberBySubscriptDate(int searchDateTime);
}
