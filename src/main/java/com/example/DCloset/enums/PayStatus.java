package com.example.DCloset.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayStatus {


    PAYMENT("결제"),
    REFUND("환불");



    private final String payType;

}
