package com.example.DCloset.model.order;

import com.example.DCloset.enums.GoodsSize;
import lombok.Getter;
import lombok.Setter;

// member id로 사이즈 컬러 바꾸기 위한 페이지

@Getter
@Setter
public class OrderChangeMemberRequest {

    private GoodsSize goodsSize;
    private String goodsColor;
}
