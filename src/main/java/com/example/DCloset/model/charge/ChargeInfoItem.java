package com.example.DCloset.model.charge;


import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.PayStatus;
import com.example.DCloset.enums.PayWay;
import com.example.DCloset.enums.PaymentAmount;
import com.example.DCloset.enums.RefundAmount;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter

public class ChargeInfoItem {


    private Long id;
    private Member member;
    private LocalDateTime actualPayDay;
    private PayStatus payStatus;
    private Double paymentAmount;
    private Double refundAmount;
    private PayWay payWay;
    private String payInfo;


}
