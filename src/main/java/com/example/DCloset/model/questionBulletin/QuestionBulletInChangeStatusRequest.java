package com.example.DCloset.model.questionBulletin;

import com.example.DCloset.enums.QuestionStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class QuestionBulletInChangeStatusRequest {

    private QuestionStatus questionStatus;
}
