package com.example.DCloset.model.questionBulletin;

import com.example.DCloset.enums.QuestionStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class QuestionBulletInCreateRequest {


    private LocalDate questionCreateDate;
    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;
    private QuestionStatus questionStatus;

}
