package com.example.DCloset.model.delivery;

import com.example.DCloset.enums.DeliveryType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class DeliveryChangeOrderRequest {

    private LocalDate deliveryDate;
    private DeliveryType deliveryType;
    private String deliveryNumber;

}
