package com.example.DCloset.controller;

import com.example.DCloset.model.CommonResult;
import com.example.DCloset.model.ListResult;
import com.example.DCloset.model.SingResult;
import com.example.DCloset.model.questionBulletin.*;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.QuestionBulletInService;
import com.example.DCloset.entity.Member;

import com.example.DCloset.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/questionBulletIn")
@CrossOrigin(origins = "*")
public class QuestionBulletInController {
    private final QuestionBulletInService questionBulletInService;
    private final MemberService memberService;

    // 등록 C
    @PostMapping ("/new/member-id/{memberId}")
    public CommonResult setQuestionBulletIn(@PathVariable long memberId, @RequestBody QuestionBulletInCreateRequest request){
        Member member = memberService.getMember(memberId);
        questionBulletInService.setQuestionBulletIn(member,request);
        return ResponseService.getSuccessResult();
    }

    // 복수 R
    @GetMapping("/list/all")
    public ListResult<QuestionBulletInItem> getQuestionBulletIns()
    {
        return ResponseService.getListResult(questionBulletInService.getQuestionBulletIns());
    }

    //단수 R
    @GetMapping("/detail/{id}")
    public SingResult getQuestionBulletIn (@PathVariable long id)
    {return ResponseService.getSingResult(questionBulletInService.getQuestionBulletIn(id));}

    @GetMapping("/detail/member/{memberId}")
    public SingResult getQuestionMemberBulletIn(@PathVariable long memberId)
    {   Member member = memberService.getMember(memberId);
        return ResponseService.getSingResult(questionBulletInService.getQuestionMemberBulletIn(member));}


    // id로 수정 하는 U
    @PutMapping("/change/correct-id/{id}")
        public CommonResult putQuestionBulletInChangeRequest(@PathVariable long id, @RequestBody QuestionBulletInChangeRequest request){
        questionBulletInService.putQuestionBulletInChangeRequest(id, request);

        return ResponseService.getSuccessResult();
    }

    // MemberId로 수정 하는 U
    @PutMapping("/correct/change-member/{memberId}")
    public CommonResult putQuestionBulletInChangeMemberRequest(@PathVariable long memberId, @RequestBody QuestionBulletInChangeMemberRequest request){
        Member member = memberService.getMember(memberId);
        questionBulletInService.putQuestionBulletInChangeMemberRequest(member, request);
        return ResponseService.getSuccessResult();
    }



    //id로 답변 상태 수정 할 수 있는
    @PutMapping("/change/status/change-id/{id}")
    public CommonResult putQuestionBulletInChangeStatusRequest(@PathVariable long id, @RequestBody QuestionBulletInChangeStatusRequest request){
        questionBulletInService.putQuestionBulletInChangeStatusRequest (id , request);

        return ResponseService.getSuccessResult();
    }



    // MemberId로 답변 상태 수정 할 수 있는 U
    @PutMapping("/correct/status/change-member/{memberId}")
    public CommonResult putQuestionBulletInChangeMemberStatusRequest(@PathVariable long memberId, @RequestBody QuestionBulletInChangeMemberStatusRequest request){
        Member member = memberService.getMember(memberId);
        questionBulletInService.putQuestionBulletInChangeMemberStatusRequest(member,request );

        return ResponseService.getSuccessResult();
    }


    // 삭제 하는 D
    @DeleteMapping("/delete/{memberId}")
    public CommonResult delQuestionBulletIn(@PathVariable long memberId){
        questionBulletInService.delQuestionBulletIn(memberService.getMember(memberId));

        return ResponseService.getSuccessResult();
    }




}
